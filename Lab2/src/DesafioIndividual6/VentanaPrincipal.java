package DesafioIndividual6;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;

public class VentanaPrincipal extends JFrame{
	public VentanaPrincipal(){
		
		
		//posicion y tama�o
		//ventanaPrincipal.setBounds(x, y, width, height);
		setBounds(500, 200, 500, 500);
		
		//cerrar ventana
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		//visibilidad
		setVisible(true);

		//icono
		Toolkit sistema = Toolkit.getDefaultToolkit();
        Image icono = sistema.getImage("diamante.png");
        setIconImage(icono);
        
        Panel panel = new Panel();
        add(panel);
        addMouseListener(new eventosMouse());
	}
}
class eventosMouse implements MouseListener {

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		System.out.println("Ocurrio un evento (click en la ventana)");
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

   
}