package DesafioIndividual6;

import java.awt.Graphics;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Panel extends JPanel{
	
	private JTextField campoTexto1, campoTexto2, campoTexto3;
	private JLabel campo1, campo2, campo3;
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		campoTexto1 = new JTextField();
		campoTexto2 = new JTextField();
		campoTexto3 = new JTextField();
		
		campoTexto1.setBounds(100, 20, 100, 20);
		campoTexto2.setBounds(100, 60, 100, 20);
		campoTexto3.setBounds(100, 100, 100, 20);
		
		add(campoTexto1);
		add(campoTexto2);
		add(campoTexto3);
		
		campo1 = new JLabel("Campo 1:");
		campo2 = new JLabel("Campo 2:");
		campo3 = new JLabel("Campo 3:");
		
		campo1.setBounds(45, 17, 100, 20);
		campo2.setBounds(45, 57, 100, 20);
		campo3.setBounds(45, 97, 100, 20);
		
		add(campo1);
		add(campo2);
		add(campo3);
		
		eventoFoco foco = new eventoFoco();
		campoTexto1.addFocusListener(foco);
		campoTexto2.addFocusListener(foco);
		campoTexto3.addFocusListener(foco);
	}
	
	class eventoFoco implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {
            if(e.getSource() == campoTexto1){
                System.out.println("El cuadro 1 tiene el foco");
            }else if(e.getSource() == campoTexto2){
                System.out.println("El cuadro 2 tiene el foco");
            }else {
            	System.out.println("El cuadro 3 tiene el foco");
            }

        }

        @Override
        public void focusLost(FocusEvent e) {
        	if(e.getSource() == campoTexto1){
                System.out.println("El cuadro 1 perdio el foco");
            }else if(e.getSource() == campoTexto2){
                System.out.println("El cuadro 2 perdio el foco");
            }else {
            	System.out.println("El cuadro 3 perdio el foco");
            }
        }
    }
	
}
