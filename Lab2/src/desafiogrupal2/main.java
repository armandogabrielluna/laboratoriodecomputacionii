package desafiogrupal2;

import java.util.HashSet;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Estudiante pibe = new Estudiante("armando", "luna", 123);
		System.out.println(pibe);
		
		Profesor profe = new Profesor("Facundo", "Matoff", 3333);
		System.out.println(profe);
		
		HashSet<Estudiante> coleccion = new HashSet<Estudiante>();
		Materia programacion = new Materia("Programacion �I", profe, coleccion);
		
		Materia.mostrarEstudiantes(programacion);
		Materia.agregarEstudiante(pibe, programacion);
		Materia.mostrarEstudiantes(programacion);
	}

}
