package desafiogrupal2;

public abstract class Persona {
	//---------------------------ATRIBUTOS------------------------------
	String nombre;
	String apellido;
	int legajo;
	
	//----------------------------CONSTRUCTOR------------------------------
	public Persona(String nombre, String apellido, int legajo) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.legajo = legajo;
	}
	//--------------------------GETTERS Y SETTERS---------------------
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getLegajo() {
		return legajo;
	}

	public void setLegajo(int legajo) {
		this.legajo = legajo;
	}
	//-----------------------------METODOS------------------------------
	public abstract void modificarDatos();
	public abstract String toString();
}
