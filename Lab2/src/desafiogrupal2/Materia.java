package desafiogrupal2;

import java.util.HashSet;
import java.util.Iterator;

public class Materia {
	//---------------------------ATRIBUTOS------------------------------
	private String nombre;
	private Profesor titular;
	private HashSet<Estudiante> coleccion = new HashSet<Estudiante>();
	
	//----------------------------CONSTRUCTOR------------------------------
	public Materia(String nombre, Profesor titular, HashSet<Estudiante> coleccion) {
		this.nombre = nombre;
		this.titular = titular;
		this.coleccion = coleccion;
	}
	
	//--------------------------GETTERS Y SETTERS---------------------
	public String getNombre() {
		return nombre;
	}

	public void cambiarNombre(String nombre) {
		this.nombre = nombre;
	}

	public Profesor getTitular() {
		return titular;
	}

	public void modificarTitular(Profesor titular) {
		this.titular = titular;
	}

	public HashSet<Estudiante> getColeccion() {
		return coleccion;
	}

	//-----------------------------METODOS------------------------------
	
	/*toma la coleccion de la materia pasada por parametro y 
	 * le agrega el alumno que se le pasa por parametro*/
	public static void agregarEstudiante(Estudiante estudiante, Materia materia) {
		materia.getColeccion().add(estudiante);
		System.out.println("agregado estudiante: "+estudiante+" a la materia "+materia.getNombre());
	}
	
	/*toma la coleccion de la materia pasada por parametro y usa un iterador
	 * para mostrar sus estudiantes*/
	public static void mostrarEstudiantes(Materia materia) {
		Iterator<Estudiante> it = materia.getColeccion().iterator();
		//si hay estudiantes los muestro
		if (it.hasNext()) {
			System.out.println("-----Estudiantes de "+materia.getNombre()+"-----");
			while(it.hasNext()) {
				  System.out.println(it.next());
				}
		}
		//sino informo que la materia en cuestion no tiene estudiantes
		else {
			System.out.println("-----"+materia.getNombre()+" no tiene estudiantes-----");
		}
		it = materia.getColeccion().iterator();
	}
}
