package TrabajoIndividialBanco;

import java.io.IOException;
import java.util.Iterator;
import java.util.Scanner;

public class menu {
	static cuentaCorriente cuentaLogeada;
	
	//este metodo es nuestro menu del "banco"
	static void startMenu() {
		Scanner pant = new Scanner(System.in);
		int numero, opcion=0;
		boolean bandera = true, bandera2 = true;
		System.out.println("------------BIENVENIDO AL BANCO--------------");
		System.out.println("\n�Que desea hacer?\n1.Ingresar a una cuenta\n2.Listar Cuentas\n3.Guardar Datos en un archivo Externo\n4.Cargar Datos de archivo externo\n5.Finalizar Programa.");
		do {
			System.out.println("Ingrese el numero de la operacion:");
			try {
				opcion = pant.nextInt();
				if(opcion > 5 || opcion <= 0) {
					throw new IOException();
				}else {
					bandera2 = false;
				}
			} catch (Exception e) {
				System.out.println("Numero invalido.");
			}
		} while (bandera2);
		switch (opcion) {
		case 1:
			System.out.println("**Ingreso de cuenta**");
			do {
				System.out.println("Ingrese el numero de su cuenta:");
				try {
					numero = pant.nextInt();
					bandera = !verificar(numero);
				} catch (Exception e) {
					System.out.println("Numero de cuenta invalido.");
				}
			} while (bandera);
			menu.menu();
			break;
		case 2:
			cuentaCorriente.mostrarLista();
			System.out.println("**Volviendo al menu de Inicio**");
			menu.startMenu();
			break;
		case 3:
			cuentaCorriente.guardarDatos();
			menu.startMenu();
			break;
		case 4:
			cuentaCorriente.cargarDatos();
			menu.startMenu();
			break;
		case 5:
			break;
		default:
			break;
		}
	}
	
	//Con este metodo verificamos que la cuenta exista, sino devolvemos un error
	private static boolean verificar(int numero) throws IOException {
		Iterator<cuentaCorriente> iter = cuentaCorriente.listaDeCuentas.iterator();
		while(iter.hasNext()) {
			cuentaCorriente cuentita = iter.next();
			if(cuentita.getAccountNumber() == numero) {
				System.out.println("---------------Bienvenido "+cuentita.getNameOwner()+"----------------");
				cuentaLogeada = cuentita;
				return true;
			}
		}
		throw new IOException("No se encontro el numero de cuenta");
	}
	
	//Este menu es para cuando estamos trabajando en una cuenta en particular
	static void menu(){
		Scanner pant = new Scanner(System.in);
		int numero = 0;
		boolean bandera = true;
		System.out.println("Que desea hacer?\n1.Depositar dinero\n2.Retirar Dinero\n3.Ver Saldo\n4.Ver Datos de la cuenta\n5.Realizar Transferencia a otra cuenta\n6.salir");
		do {
			System.out.println("Ingrese el numero de la operacion:");
			try {
				numero = pant.nextInt();
				if(numero > 6 || numero <= 0) {
					throw new IOException();
				}else {
					bandera = false;
				}
			} catch (Exception e) {
				System.out.println("Numero invalido.");
			}
		} while (bandera);
		switch (numero) {
		//DEPOSITAR DINERO
		case 1:
			cuentaLogeada.depositarDinero();
			System.out.println("**Volviendo al menu de opciones**");
			menu.menu();
			break;
		//RETIRAR DINERO
		case 2:
			cuentaLogeada.retirarDinero();
			System.out.println("**Volviendo al menu de opciones**");
			menu.menu();
			break;
		//VER SALDO
		case 3:
			System.out.println(cuentaLogeada.getBalance());
			System.out.println("**Volviendo al menu de opciones**");
			menu.menu();
			break;
		//VER DATOS DE LA CUENTA
		case 4:
			System.out.println("------DATOS DE LA CUENTA------");
			System.out.println(cuentaLogeada);
			System.out.println("**Volviendo al menu de opciones**");
			menu.menu();
			break;
		//REALIZAR TRANSFERENCIA
		case 5:
			cuentaLogeada.transferencia();
			menu.menu();
			break;
		//SALIR DE LA CUENTA
		case 6:
			System.out.println("**Volviendo al menu de Inicio**");
			menu.startMenu();
			break;
		default:
			break;
		}
	}
}
