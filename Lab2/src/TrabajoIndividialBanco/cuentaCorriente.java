package TrabajoIndividialBanco;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class cuentaCorriente implements Serializable {
	
	//-------------attributes-------------
	
	String nameOwner;
	double balance = 0;
	int accountNumber;
	static int cont=1;
	static ArrayList<cuentaCorriente> listaDeCuentas = new ArrayList<cuentaCorriente>();
	//este atributo sera ocupado cuando se realicen transferencias
	static cuentaCorriente cuentaSecundaria;
	
	//--------builder--------
	
		public cuentaCorriente(String nameOwner) {
			this.nameOwner = nameOwner;
			this.accountNumber = cont;
			cont++;
			listaDeCuentas.add(this);
		}

	//---------Getters y Setters----------
	
	public String getNameOwner() {
		return nameOwner;
	}
	
	public void setNameOwner(String nameOwner) {
		this.nameOwner = nameOwner;
	}
	
	public double getBalance() {
		System.out.println("Saldo de "+this.getNameOwner());
		return balance;
	}
	
	public void setBalance(double numero) {
		this.balance = this.balance+numero;
	}

	public int getAccountNumber() {
		return accountNumber;
	}
	
	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	
	//--------methods-------------
	//este metodo nos muestra la lista de cuentas
	public static void mostrarLista() {
		System.out.println("\n----------LISTADO DE CUENTAS------");
		Iterator iter = listaDeCuentas.iterator();
		while (iter.hasNext()) {
		  System.out.println(iter.next());
		}
		System.out.println("----------------------------------\n");
	}
	
	//Con este metodo podemos retirar dinero en una cuenta
	public void retirarDinero() {
		Scanner pant = new Scanner(System.in);
		double number = 0;
		boolean fin = false;
		System.out.println("**Retiro de Dinero**");
		while(!fin) {
			do {
				try {
					System.out.println("Ingrese un valor a retirar:");
					number = pant.nextDouble();
					evaluarNumero(number);
				} catch (Exception e) {
					System.out.println("Ingreso un valor Erroneo !!!");
				}
				System.out.println();
			} while (number <= 0);
			if (number < this.balance) {
				this.balance = balance-number;
				System.out.println("Saldo restante:");
				System.out.println(this.getBalance());
				fin = true;
			}else {
				System.out.println("No tiene suficiente dinero para realizar esta operacion");
			}
		}
	}
	
	//Con este metodo podemos depositar dinero en una cuenta
	public void depositarDinero() {
		Scanner pant = new Scanner(System.in);
		double numero=0; 
		boolean bandera = true;
		System.out.println("ingrese monto a depositar");
		while (bandera) {
			try {
				numero = pant.nextDouble();
				bandera = false;
			} catch (Exception e) {
				System.out.println("Monto invalido, ingrese nuevamente");
			}
		}
		setBalance(numero);
	}
	//Con este metodo concretamos la transferencia entre 2 cuentas
	public void transferencia () {
		Scanner pant = new Scanner(System.in);
		boolean bandera = true, fin = false;
		int numeroCuenta;
		double monto=0;
		System.out.println("**Ingrese numero de Cuenta Destinataria**");
		do {
			System.out.println("Ingrese el numero de cuenta a la cual enviara dinero:");
			try {
				numeroCuenta = pant.nextInt();
				bandera = !verificar(numeroCuenta);
			} catch (Exception e) {
				System.out.println("Numero de cuenta invalido.");
			}
		} while (bandera);
		while(!fin) {
			do {
				try {
					System.out.println("Ingrese monto para realizar la transferencia:");
					monto = pant.nextDouble();
					evaluarNumero(monto);
				} catch (Exception e) {
					System.out.println("Ingreso un valor Erroneo !!!");
				}
				System.out.println();
			} while (monto <= 0);
			if (monto < this.balance) {
				this.balance = balance-monto;
				cuentaSecundaria.setBalance(monto);
				System.out.println("Saldo restante:");
				System.out.println(this.getBalance());
				fin = true;
				cuentaSecundaria = null;
			}else {
				System.out.println("No tiene suficiente dinero para realizar esta operacion");
			}
		}
	}
	
	//este metodo es utilizado por solamente el metodo transferencia para verificar
	//la cuenta a la que vamos a transferirle el dinero
	private static boolean verificar(int numero) throws IOException {
		Iterator<cuentaCorriente> iter = listaDeCuentas.iterator();
		while(iter.hasNext()) {
			cuentaCorriente cuentita = iter.next();
			if(cuentita.getAccountNumber() == numero) {
				System.out.println("Usted enviara dinero a la cuenta: "+cuentita);
				cuentaSecundaria = cuentita;
				return true;
			}
		}
		throw new IOException("No se encontro el numero de cuenta");
	}
	
	@Override
	public String toString() {
		return "cuentaCorriente [nameOwner=" + nameOwner + ", balance=" + balance + ", accountNumber=" + accountNumber
				+ "]";
	}
	
	//este metodo nos dice si el numero que se ingreso para realizar
	//una operacion con el monto es negativo o nulo, de ser asi devuelve un error
	static void evaluarNumero(double number) throws IOException {
		if (number <=0 ) {
			throw new IOException("Numero menor o igual a 0");
		}
	}
	
	public static void guardarDatos () {
		try
        {
			ObjectOutputStream escribiendoFichero = new ObjectOutputStream(new FileOutputStream("objetos.dat") );
		    escribiendoFichero.writeObject(listaDeCuentas);
		    escribiendoFichero.close();
		    System.out.println("DATOS GUARDADOS");
        } 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void cargarDatos () {
		try
        {
			ObjectInputStream leyendoFichero = new ObjectInputStream(new FileInputStream("objetos.dat") );
		    listaDeCuentas = (ArrayList<cuentaCorriente>) leyendoFichero.readObject();
		    leyendoFichero.close();
		    System.out.println("DATOS CARGADOS");
        } 
        catch (IOException ioe) 
        {
            ioe.printStackTrace();
            return;
        } 
        catch (ClassNotFoundException c) 
        {
            System.out.println("Class not found");
            c.printStackTrace();
            return;
        }
		
	}
	
}
