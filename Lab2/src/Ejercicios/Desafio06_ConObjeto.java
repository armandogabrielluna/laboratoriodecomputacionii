package Ejercicios;

public class Desafio06_ConObjeto {
//utilizo la clase fracciones
	public static void main(String[] args) {
		// definimos cada una de las fracciones que vemos en el ejercicio combinado
		fracciones frac1 = new fracciones(-2,3);
		fracciones frac2 = new fracciones(7,2);
		fracciones frac3 = new fracciones(2,30);
		fracciones frac4 = new fracciones(6,5);
		fracciones frac5 = new fracciones(1,3);
		fracciones frac6 = new fracciones(5);
		//luego comenzamos a operar y mostramos el resultado de la operacion
		System.out.println(frac1.multiplicar(frac2).sumar(frac3).sumar(frac4).dividir(frac5.dividir(frac6)));
	}
	
}
