package Ejercicios;

import java.util.Scanner;

public class Desafio09 {
	
public static void main(String[] args) {
	float altura, pesoideal, idealH = 120, idealM=110;
	String genero;
	Scanner teclado = new Scanner(System.in);
	System.out.println("Ingrese su altura en cm (solo numero)");
	altura = teclado.nextFloat();
	System.out.println("Ingrese su genero, H para hombre y M para mujer");
	genero = teclado.next().toUpperCase();
	teclado.close();
	if (genero.equals("H")) {
		pesoideal = altura-idealH;
		System.out.println("Su peso ideal es "+pesoideal+"kg.");
	}
	else if (genero.equals("M")) {
		pesoideal = altura-idealM;
		System.out.println("Su peso ideal es "+pesoideal+"kg.");
	}
	else {
		System.out.println("Ingreso de forma incorrecta su genero");
	}
	
}
}
