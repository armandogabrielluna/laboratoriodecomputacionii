package Ejercicios;

import java.util.Scanner;

public class Ejercicio13_3 {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		int cantPersonas;
		System.out.println("-------CARGA DE PERSONAS-----");
		System.out.println("Cuantas personas va a ingresar?");
		cantPersonas = teclado.nextInt();
		//sumamos 1 a las personas para comenzar luego de los "titulos"
		cantPersonas++;
		String matriz[][] = new String[3][cantPersonas];
		//harcodeamos los titulos
		matriz[0][0]="NOMBRE";
		matriz[1][0]="DNI   ";
		matriz[2][0]="EDAD  ";
		//llamamos al metodo para cargar una matriz
		CargarMatriz(matriz);
		System.out.println("-------TABLA DE PERSONAS-----");
		//llamamos al metodo para mostrar una matriz
		MostrarMatriz(matriz);
		OrdenarPersonas(matriz);
		teclado.close();
	}
	//metodo estatico de clase, nos muestra una matriz
	public static void MostrarMatriz(String matriz[][]) {
		//el primer for recorre las filas
		for (int filas=0; filas < matriz.length; filas++) {
			  System.out.print("|");
			  //este for recorre las columnas
			  for (int columnas=0; columnas < matriz[filas].length; columnas++) {
				  System.out.print (matriz[filas][columnas]);
				  System.out.print("|");
				  System.out.print("\t");
			  }
			  System.out.print("\n");
		}
	}
	//metodo estatico de clase, este metodo carga la matriz de personas
	public static void CargarMatriz (String matriz[][]) {
		//empezamos desde la columna 1 ya que la columna 0 es de los titulos
		int filas=0, columnas=1;
		String nombre;
		Scanner pant = new Scanner(System.in);
		while (columnas < matriz[filas].length) {
			System.out.println("Ingrese nombre de la persona N�"+columnas+":");
			matriz[filas][columnas] = pant.next();
			nombre = matriz[filas][columnas];
			AutoCompletar(filas, columnas, matriz);
			filas++;
			System.out.println("Ingrese DNI de "+nombre+":");
			matriz[filas][columnas] = pant.next();
			AutoCompletar(filas, columnas, matriz);
			filas++;
			System.out.println("Ingrese la edad de "+nombre+":");
			matriz[filas][columnas] = pant.next();
			AutoCompletar(filas, columnas, matriz);
			filas = 0;
			columnas++;
		}
		pant.close();
	}
	//este metodo rellena strings hasta la longitud 15 en cada valor del arreglo
	public static void AutoCompletar(int fila,int columna,String matriz[][]) {
		String palabra = matriz[fila][columna];
		if(palabra.length() < 15) {
			int iter = 15-palabra.length();
			for (int i = 0; i < iter; i++) {
				palabra=palabra+" ";
			}
			matriz[fila][columna] = palabra;
		}
	}
	
	//metodo para ordenar los nombres y sus datos
	public static void OrdenarPersonas(String matriz[][]) {
		int filasLimite = matriz.length, columnasLimite = matriz[0].length, columnas = 1, fila=0, base=1;
		String aux;
		if (columnasLimite == 2) {
			System.out.println("No es necesario ordenar");
		}
		else {
			//definimos un while, base sera la posicion del "pivot" de comparacion
			while (base != columnasLimite-1) {
				/*si la columna (nuestro pivot movil) llego al final, 
				 * base avanza 1 posicion y la columna toma el valor de base
				 */
				if (columnas == columnasLimite) {
					base++;
					columnas = base;
				}
				/*si el metodo comparteTo.. devuelve valor positivo si mayor si el 
				 * valor de la cadena es mayor que el valor de la cadena pasada por par�metro 
				 */
				if(matriz[fila][base].compareToIgnoreCase(matriz[fila][columnas]) > 0) {
					aux = matriz[fila][columnas];
					matriz[fila][columnas] = matriz[fila][base];
					matriz[fila][base] = aux;
					//este while se encarga de cambiar los valores de las filas "dni" y "edad"
					fila++;
					while (fila < filasLimite ) {
						aux = matriz[fila][columnas];
						matriz[fila][columnas] = matriz[fila][base];
						matriz[fila][base] = aux;
						fila++;
					}
					fila = 0;
					columnas++;
				}
				else {
					columnas++;
				}
			}
			System.out.println("-------TABLA DE PERSONAS ORDENADA-----");
			MostrarMatriz(matriz);
		}
	}
}