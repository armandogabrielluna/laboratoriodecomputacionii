package Ejercicios;

import java.util.Scanner;

public class Desafio07 {

	public static void main(String[] args) {
		double opuesto, hipotenusa, adyacente, numero;
		Scanner teclado = new Scanner(System.in);
		System.out.println("Vamos a trabajar con las medidas de un triangulo");
		System.out.println("Ingresa la hipotenusa");
		hipotenusa = teclado.nextDouble();
		System.out.println("Ingresa la medida del lado opuesto");
		opuesto = teclado.nextDouble();
		System.out.println("Ingresa la medida del lado adyacente");
		adyacente = teclado.nextDouble();
		System.out.println("El seno es: "+ Math.sin(opuesto/hipotenusa));
		System.out.println("El Coseno es "+Math.cos(adyacente/hipotenusa));
		System.out.println("La tangente es "+Math.tan(opuesto/adyacente));
		
		System.out.println("Usando Math.atan con el valor 10 :"+Math.atan(10));
		System.out.println("Usando Math.atan2 con el valor 10 y 20 :"+Math.atan2(10, 20));
		
		System.out.println("Ingresa un numero para calcular su exponencial y su logaritmo en base e");
		numero = teclado.nextDouble();
		System.out.println("El exponencia de "+numero+" es "+Math.exp(numero));
		System.out.println("El logaritmo de "+numero+" es "+Math.log(numero));
		
		System.out.println("PI es "+Math.PI);
		System.out.println("E es "+Math.E);

	}

}
