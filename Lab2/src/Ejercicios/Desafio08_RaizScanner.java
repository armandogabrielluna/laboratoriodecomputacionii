package Ejercicios;

import java.util.Scanner;

public class Desafio08_RaizScanner {
	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		float numero;
		System.out.println("Ingresar un numero para calcular su raiz cuadrada");
		numero = teclado.nextFloat();
		System.out.println("La raiz de "+numero+" es = "+Math.sqrt(numero));
	}
}
