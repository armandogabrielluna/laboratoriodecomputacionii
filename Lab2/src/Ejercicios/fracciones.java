package Ejercicios;
// clase para resolver el Desafio06
public class fracciones {
	int numerador, denominador;
	
	//constructor
	public fracciones (int num, int deno) {
		this.numerador = num;
		this.denominador = deno;
	}
	//constructor
	public fracciones (int num) {
		this.numerador = num;
		this.denominador = 1;
	}
	//constructor
	public fracciones () {
		this.numerador = 0;
		this.denominador = 1;
	}
	
	
	//getters y setters
	public int getNumerador() {
		return numerador;
	}

	public void setNumerador(int numerador) {
		this.numerador = numerador;
	}

	public int getDenominador() {
		return denominador;
	}

	public void setDenominador(int denominador) {
		this.denominador = denominador;
	}
	
	//metodo para buscar el maximo comun denominador
		public int maximoComunDenominador () {
			int num = Math.abs(this.numerador);
			int deno = Math.abs(this.denominador);
			if (num == 1) {
				return 1;
			}else {
				if (deno>num) {
					num = num + deno;
					deno = num - deno;
					num = num - deno;
				}
				int aux=1;
				while (deno!= 0) {
					aux = deno;
					deno = num % deno;
					num = aux;
				}
				return aux;
			}
		}
		
	//metodo para simplificar, necesita usar el metodo de maximo comun denominador
		public void simplificar () {
			int mcd = maximoComunDenominador();
			numerador = numerador / mcd;
			denominador = denominador / mcd;
			if(numerador<0 && denominador<0) {
				numerador = numerador * -1;
				denominador = denominador * -1;
			}else if(numerador>0 && denominador <0) {
				numerador = numerador * -1;
				denominador = denominador * -1;
			}
		}
	
	//metodo para poder sumar 2 fracciones
	public fracciones sumar(fracciones fraccion2) {
		fracciones suma = new fracciones (numerador*fraccion2.denominador+denominador*fraccion2.numerador,
				denominador*fraccion2.denominador);
		suma.simplificar();
		return suma;
	}
	//metodo para poder restar 2 fracciones
	public fracciones restar(fracciones fraccion2) {
		fracciones resta = new fracciones (numerador*fraccion2.denominador-denominador*fraccion2.numerador,
				denominador*fraccion2.denominador);
		resta.simplificar();
		return resta;
	}
	//metodo para poder multiplicar 2 fracciones
	public fracciones multiplicar(fracciones fraccion2) {
		fracciones multiplico = new fracciones (numerador*fraccion2.numerador, denominador*fraccion2.denominador);
		multiplico.simplificar();
		return multiplico;
	}
	//metodo para poder dividir 2 fracciones
	public fracciones dividir(fracciones fraccion2) {
		fracciones divido = new fracciones (numerador*fraccion2.denominador, denominador*fraccion2.numerador);
		divido.simplificar();
		return divido;
	}
	
	
	//metodo para mostrar o "printear" el objeto fracciones
	@Override
	public String toString() {
		if (denominador != 0){
			if (denominador != 1) {
				simplificar();
				return numerador+"/"+denominador;
			}else {
				return numerador+"";
			}
			
		}
		else {
			return "el denominador debe ser distinto de 0";
		}
		
	}
}
