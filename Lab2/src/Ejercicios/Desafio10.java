package Ejercicios;

import java.util.Scanner;

public class Desafio10 {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		int ranNum = (int) Math.random()*11, num=0, intentos = 0 ;
		do {
			System.out.println("Ingrese un numero");
			num = teclado.nextInt();
			if(num	> ranNum) {
				System.out.println("El numero que ingreso es menor");
			}else if (num < ranNum){
				System.out.println("el numero que ingreso es mayor");
			}else {
				System.out.println("Correcto!");
			}
			intentos++;
			System.out.println("Intentos realizados :"+intentos);
		} while (num != ranNum);
		System.out.println("El numero generado fue: "+ranNum);
	}
}
