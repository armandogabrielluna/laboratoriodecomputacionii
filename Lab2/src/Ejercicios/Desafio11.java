package Ejercicios;

import java.util.Random;

public class Desafio11 {
	
	public static void main(String[] args) {
		CuentaBancaria obj1 = new CuentaBancaria("Pedrito", 3000.0);
		CuentaBancaria obj2 = new CuentaBancaria("Jose", 4000.0);
		System.out.println("-----------Cuentas---------------");
		System.out.println(obj1);
		System.out.println(obj2);
		System.out.println("----------Transferencia-----------");
		System.out.println("Monto de la transferencia $500.5");
		CuentaBancaria.Transferencias(obj1, obj2, 500.50);
	}
}

class CuentaBancaria{
	private String nombreAfiliado;
	private double saldo;
	private long numeroCuenta;
	
	public CuentaBancaria(String nombreAfiliado, double saldo) {
		this.nombreAfiliado = nombreAfiliado;
		this.saldo = saldo;
		
		Random aleatorio = new Random();
		
		this.numeroCuenta = Math.abs(aleatorio.nextLong());
	
	}
	//setter para ingresar dinero a la cuenta (saldo)
	public String ingresarDinero(double dinero) {
		if(dinero>0) {
			this.saldo += dinero;
			return "Se ingresaron correctamente los $"+dinero;
		}else {
			return "No se puede ingresar valores negativos";
		}
	}
	//setter para quitar dinero a la cuenta (saldo)
	public void sacarDinero(double dinero) {
		this.saldo -= dinero;
	}
	//get para obtener el saldo de la cuenta
	public double getSaldo() {
		return saldo;
	}
	//get para obtener el numero de cuenta
	public String getNombreAfiliado() {
		return nombreAfiliado;
	}
	@Override
	public String toString() {
		return "CuentaBancaria [nombreAfiliado=" + nombreAfiliado + 
				", saldo=" + saldo + ", numeroCuenta="
				+ numeroCuenta + "]";
	}
	// Metodo para generar una transferencia de dinero entre 2 cuentas
	public static void Transferencias(CuentaBancaria CuentaRecibe,CuentaBancaria CuentaPierde, double cantidad) {
		CuentaPierde.saldo -= cantidad;
		CuentaRecibe.saldo += cantidad;
		System.out.println("Se transfirieron exitosamente $"+cantidad+" de la cuenta "+CuentaPierde+" a la cuenta "+CuentaRecibe);
	}
	
}
