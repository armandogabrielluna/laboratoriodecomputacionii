package TtrabajoIndividialProcesadorDeTexto;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowStateListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class VentanaPrincipal extends JFrame{
	public VentanaPrincipal() {
		
		//--------------------TITULO--------------------
		setTitle("Editor de textos by Luna Armando");
		
		//--------------------DIMENCIONES--------------------
        setBounds(600, 350, 600, 300);
        
        //--------------------PANEL--------------------
        PanelPrincipal editorTexto = new PanelPrincipal();
        add(editorTexto);
        
        //--------------------ICONO--------------------
        Toolkit sistema = Toolkit.getDefaultToolkit();
        Image icono = sistema.getImage("utn.png");
        setIconImage(icono);
        
        
        //--------------------VISIBILIDAD--------------------
        setVisible(true);
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        addWindowListener(new EventosDeVentana());
        
	}
	
	private class EventosDeVentana implements WindowListener{

		@Override
		public void windowOpened(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowClosing(WindowEvent e) {
			if (!PanelPrincipal.guardado) {
				int reply = JOptionPane.showConfirmDialog(null, "�Desea Guardar?", "Esta Saliendo sin guardar", JOptionPane.YES_NO_OPTION);
				if (reply == JOptionPane.YES_OPTION) {
					if (PanelPrincipal.guardado) {
						PanelPrincipal.guardarDatosRapido();
					} else {
						PanelPrincipal.guardarDatos();
					}
				}
				else {
					System.exit(0);
				}
				
			}	
		}

		@Override
		public void windowClosed(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowIconified(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowDeiconified(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowActivated(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowDeactivated(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
}
