package TtrabajoIndividialProcesadorDeTexto;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.StyledEditorKit;



public class PanelPrincipal extends JPanel{
	
	private static JTextPane areaTexto;
	
	
	public PanelPrincipal() {
		
		
		setLayout(new BorderLayout());
		
		//--------------------MENUS--------------------
		JMenu archivo = new JMenu("Archivo");
		JMenu fuente = new JMenu("Fuente");
		JMenu estilo = new JMenu("Estilo");
		JMenu tamanio = new JMenu("Tama�o");
		JMenu alineacion = new JMenu("Alineaci�n");
		JMenu color = new JMenu("Colores");
		JMenu ayuda = new JMenu("Ayuda");
		
		//--------------------ITEMS DE CADA MENU--------------------
		
		//**********MENU DE ARCHIVOS**********
		
		//creo los items
		JMenuItem abrir = new JMenuItem("Abrir...");
		JMenuItem guardar = new JMenuItem("Guardar");
		
		//a�ado los items al menu de archivos
		archivo.add(abrir);
		archivo.add(guardar);
		
		//listeners
		abrir.addActionListener(new escuchandoAlAbrir());
		guardar.addActionListener(new escuchandoAlGuardar());
		guardar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_DOWN_MASK));
		
		//**********MENU DE FUENTES**********
		
		//creo los items
		JMenuItem arial = new JMenuItem("Arial");
		JMenuItem courier = new JMenuItem("Courier");
		JMenuItem verdana = new JMenuItem("Verdana");
		JMenuItem timesNewRoman = new JMenuItem("Times New Roman");
		JMenuItem tahoma = new JMenuItem("Tahoma");
		
		//a�ado los items al menu de fuentes
		fuente.add(arial);
		fuente.add(courier);
		fuente.add(verdana);
		fuente.add(timesNewRoman);
		fuente.add(tahoma);
		
		//Listeners Fuentes (a�ado listeners a cada item del menu)
		arial.addActionListener(new StyledEditorKit.FontFamilyAction("cambioDeLetra","Arial"));
		courier.addActionListener(new StyledEditorKit.FontFamilyAction("cambioDeLetra","Courier"));
		verdana.addActionListener(new StyledEditorKit.FontFamilyAction("cambioDeLetra","Verdana"));
		timesNewRoman.addActionListener(new StyledEditorKit.FontFamilyAction("cambioDeLetra","Times New Roman"));
		tahoma.addActionListener(new StyledEditorKit.FontFamilyAction("cambioDeLetra","Tahoma"));
		
		//**********MENU DE ESTILOS**********
		
		//creo los items
		JMenuItem negrita = new JMenuItem("Negrita");
		JMenuItem cursiva = new JMenuItem("Cursiva");
		JMenuItem subrayado = new JMenuItem("Subrayado");
		
		//a�ado los items al menu de estilos
		estilo.add(negrita);
		estilo.add(cursiva);
		estilo.add(subrayado);
		
		//Listener Estilo (a�ado a cada item un listener y una funcion para definir una combinacion de teclas al item) 
		negrita.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
		negrita.addActionListener(new StyledEditorKit.BoldAction());
		cursiva.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K,InputEvent.CTRL_DOWN_MASK));
		cursiva.addActionListener(new StyledEditorKit.ItalicAction());
		subrayado.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
		subrayado.addActionListener(new StyledEditorKit.UnderlineAction());
		
		
		//**********MENU DE TAMANIOS**********
		
		//grupo de botones para solo poder seleccionar 1 opcion
		ButtonGroup grupoDeTamanios = new ButtonGroup();
		
		//creo los botones
		JRadioButtonMenuItem tam8 = new JRadioButtonMenuItem("8");
		JRadioButtonMenuItem tam10 = new JRadioButtonMenuItem("10");
		JRadioButtonMenuItem tam12 = new JRadioButtonMenuItem("12");
		JRadioButtonMenuItem tam14 = new JRadioButtonMenuItem("14");
		JRadioButtonMenuItem tam16 = new JRadioButtonMenuItem("16");
		JRadioButtonMenuItem tam18 = new JRadioButtonMenuItem("18");
		JRadioButtonMenuItem tam20 = new JRadioButtonMenuItem("20");
		JRadioButtonMenuItem tam22 = new JRadioButtonMenuItem("22");
		JRadioButtonMenuItem tam24 = new JRadioButtonMenuItem("24");
		
		//a�ado los botones al grupo de botones
		grupoDeTamanios.add(tam8);
		grupoDeTamanios.add(tam10);
		grupoDeTamanios.add(tam12);
		grupoDeTamanios.add(tam14);
		grupoDeTamanios.add(tam16);
		grupoDeTamanios.add(tam18);
		grupoDeTamanios.add(tam20);
		grupoDeTamanios.add(tam22);
		grupoDeTamanios.add(tam24);
		
		//a�ado los botones al menu de tamanio
		tamanio.add(tam8);
		tamanio.add(tam10);
		tamanio.add(tam12);
		tamanio.add(tam14);
		tamanio.add(tam16);
		tamanio.add(tam18);
		tamanio.add(tam20);
		tamanio.add(tam22);
		tamanio.add(tam24);
		
		
		//Listeners Tamanio (a�ado listeners a cada boton)
		tam8.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 8));
		tam10.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 10));
		tam12.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 12));
		tam14.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 14));
		tam16.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 16));
		tam18.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 18));
		tam20.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 20));
		tam22.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 22));
		tam24.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 24));
		
		//**********MENU DE ALINEACION**********
		
		//creo los items
		JMenuItem alinearIzquierda = new JMenuItem("Alinear Izquierda");
		JMenuItem centrar = new JMenuItem("Centrar");
		JMenuItem alinearDerecha = new JMenuItem("alinear Derecha");
		JMenuItem justificar = new JMenuItem("Justificar");
		
		//a�ado los items al menu de alineacion
		alineacion.add(alinearIzquierda);
		alineacion.add(centrar);
		alineacion.add(alinearDerecha);
		alineacion.add(justificar);
		
		//Listener Alineacion (a�ado listeners a cada item y teclas)
		alinearIzquierda.addActionListener(new StyledEditorKit.AlignmentAction("cambiarAlineacion", StyleConstants.ALIGN_LEFT));
		alinearIzquierda.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_DOWN_MASK));
		centrar.addActionListener(new StyledEditorKit.AlignmentAction("cambiarAlineacion", StyleConstants.ALIGN_CENTER));
		centrar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.CTRL_DOWN_MASK));
		alinearDerecha.addActionListener(new StyledEditorKit.AlignmentAction("cambiarAlineacion", StyleConstants.ALIGN_RIGHT));
		alinearDerecha.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_DOWN_MASK));
		justificar.addActionListener(new StyledEditorKit.AlignmentAction("cambiarAlineacion", StyleConstants.ALIGN_JUSTIFIED));
		justificar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_J, InputEvent.CTRL_DOWN_MASK));
		
		//**********MENU DE COLORES**********
		
		//creo los items
		JButton azul = new JButton();
		JButton rojo = new JButton();
		JButton verde = new JButton();
		JButton naranja = new JButton();
		JButton amarillo = new JButton();
		JButton negro = new JButton();
		JButton morado = new JButton();
		azul.setBackground(Color.BLUE);
		rojo.setBackground(Color.RED);
		verde.setBackground(Color.GREEN);
		naranja.setBackground(Color.ORANGE);
		amarillo.setBackground(Color.YELLOW);
		negro.setBackground(Color.BLACK);
		morado.setBackground(Color.MAGENTA);
		
		//a�ado los colores botones al menu de colores
		color.add(negro);
		color.add(azul);
		color.add(morado);
		color.add(rojo);
		color.add(naranja);
		color.add(amarillo);
		color.add(verde);
		
		//listeners Colores (a�ado listeners a los botones)
		azul.addActionListener(new StyledEditorKit.ForegroundAction("cambiarColor", Color.BLUE));
		rojo.addActionListener(new StyledEditorKit.ForegroundAction("cambiarColor", Color.RED));
		verde.addActionListener(new StyledEditorKit.ForegroundAction("cambiarColor", Color.GREEN));
		naranja.addActionListener(new StyledEditorKit.ForegroundAction("cambiarColor", Color.ORANGE));
		amarillo.addActionListener(new StyledEditorKit.ForegroundAction("cambiarColor", Color.YELLOW));
		negro.addActionListener(new StyledEditorKit.ForegroundAction("cambiarColor", Color.BLACK));
		morado.addActionListener(new StyledEditorKit.ForegroundAction("cambiarColor", Color.MAGENTA));
		
		//**********MENU DE AYUDA**********
		
		//creo los items
		JMenuItem verAyuda = new JMenuItem("Ver Ayuda");
		
		//a�ado el item al menu de ayuda
		ayuda.add(verAyuda);
		
		//listener de ayuda
		verAyuda.addActionListener(new ayuditaEscucha());
		
		//--------------------BARRA DE MENUS--------------------
		JMenuBar barraMenu = new JMenuBar();
		barraMenu.add(archivo);
		barraMenu.add(fuente);
		barraMenu.add(estilo);
		barraMenu.add(alineacion);
		barraMenu.add(tamanio);
		barraMenu.add(color);
		barraMenu.add(ayuda);
		
		//--------------------AREA DE TEXTO--------------------
		areaTexto = new JTextPane();
		//tenemos que meter el area de texto en un Scroll, luego solo basta a�adir el scroll al panel
		JScrollPane scroll = new JScrollPane(areaTexto);
		
		//--------------------ADDS AL PANEL--------------------
		add(barraMenu, BorderLayout.NORTH);
		add(scroll, BorderLayout.CENTER);
		
	}
	
	//en estas variables se guardan las rutas del guardado para poder acceder al "guardadoRapido"
	private static String rutaGuardada = "";
	private static String rutaGuardadaStyle = "";
	//este boleano se ocupara para saber si el archivo nuevo fue guardado o no
	public static boolean guardado = false;
	
	//Metodo para cargar datos
	public static void cargarDatos () {
		try
        {
			JFileChooser elegirArchivo = new JFileChooser();
			
			//abrimos el FileChoose sobre el JTextPane
			elegirArchivo.showOpenDialog(areaTexto);
			
			//guardamos la direccion del archivo
			String archivo = elegirArchivo.getSelectedFile().getAbsolutePath();
			
			//direccion del archivo style
			String archivoSyle = elegirArchivo.getCurrentDirectory() +"\\"+ elegirArchivo.getSelectedFile().getName() + "Style";
			
			ObjectInputStream leyendoFichero = new ObjectInputStream(new FileInputStream(archivo));
			ObjectInputStream leyendoFicheroStyle = new ObjectInputStream(new FileInputStream(archivoSyle));
			
			//leemos el archivo para traer el texto
			areaTexto.setText((String) leyendoFichero.readObject());
			//leemos el archivoStyle para traer el formato
			areaTexto.setStyledDocument((StyledDocument) leyendoFicheroStyle.readObject());
			
			leyendoFichero.close();
		    leyendoFicheroStyle.close();
		    guardado = false;
        } 
        catch (IOException ioe) 
        {
        	JOptionPane.showConfirmDialog(null, "ACCESO DENEGADO", "Error", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
            ioe.printStackTrace();
            return;
        } 
        catch (ClassNotFoundException c) 
        {
            System.out.println("Class not found");
            c.printStackTrace();
            return;
        }
		
	}
	
	//Listener especialmente para el item Abrir del menu archivo
	private class escuchandoAlAbrir implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			//Directamente llama al metodo cargarDatos
			PanelPrincipal.cargarDatos();
		}
	}
	
	//metodo para la primera vez que se va a guardar el archivo
	public static void guardarDatos () {
		try
        {
			JFileChooser elegirArchivo = new JFileChooser();
			elegirArchivo.showSaveDialog(areaTexto);
			
			//primero tomamos la direccion
			File archivo = (File) elegirArchivo.getCurrentDirectory();
			
			//usamos la direccion anterior para poder generar el path completo
			String ruta = archivo+"\\"+elegirArchivo.getSelectedFile().getName();
			
			//usamos la direccion anterior para poder generar el path completo para el archivo style
			String rutaStyle = archivo+"\\"+elegirArchivo.getSelectedFile().getName()+"Style";
			
			//guardamos esas rutas en nuestras variables estaticas para poder utilizarlas luego
			rutaGuardada = ruta;
			rutaGuardadaStyle = rutaStyle;
			
			ObjectOutputStream escribiendoFichero = new ObjectOutputStream(new FileOutputStream(ruta) );
			ObjectOutputStream escribiendoFicheroStyles = new ObjectOutputStream(new FileOutputStream(rutaStyle) );
		    
			//guardamos el texto
			escribiendoFichero.writeObject(areaTexto.getText());
		    
			//guardamos un StyleDocument
			escribiendoFicheroStyles.writeObject(areaTexto.getStyledDocument());
		    escribiendoFichero.close();
		    escribiendoFicheroStyles.close();
		    
		    JOptionPane.showConfirmDialog(null, "Archivos Guardados", "Exito", JOptionPane.CLOSED_OPTION, JOptionPane.PLAIN_MESSAGE);
		    guardado = true;
        } 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	//metodo para soobrescribir hacer guardados sobre los archivos ya definidos
	public static void guardarDatosRapido () {
		try
        {
			//aqui podemos usar las variables del anterior guardado para directamente sobreescribir los archivos
			ObjectOutputStream escribiendoFichero = new ObjectOutputStream(new FileOutputStream(rutaGuardada) );
			ObjectOutputStream escribiendoFicheroStyles = new ObjectOutputStream(new FileOutputStream(rutaGuardadaStyle) );
			
			escribiendoFichero.writeObject(areaTexto.getText());
			escribiendoFicheroStyles.writeObject(areaTexto.getStyledDocument());
			
			escribiendoFicheroStyles.close();
		    escribiendoFichero.close();
        } 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	//metodo que escucha solamente al item Guardar del menu archivos
	private class escuchandoAlGuardar implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			if (guardado) {
				PanelPrincipal.guardarDatosRapido();
			} else {
				PanelPrincipal.guardarDatos();
			}
			
		}
	}
	
	//metodo que escucha solamente al item de "ver ayuda" en el menu de ayuda 
	private class ayuditaEscucha implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			JOptionPane.showConfirmDialog(null, "Si creas un archivo por ejemplo, (nombredearchivo)\n vas a ver una copia del archivo con un agregado en el nombre (nombredearchivoStyle)\n nunca selecciones este archivo ya que es el que guarda el formato del archivo original\n es decir (nombredearchivo).\n Recorda, solamente selecciona el archivo sin la palabra 'Style' en el nombre!!!!\n ESTE SI: nombreEjemplo\nESTE NO:nombreEjemploStyle", "GUARCAR/CARGAR ARCHIVOS", JOptionPane.CLOSED_OPTION, JOptionPane.INFORMATION_MESSAGE);
		}
		
	}
}
