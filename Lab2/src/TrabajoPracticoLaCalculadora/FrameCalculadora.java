 package TrabajoPracticoLaCalculadora;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;

public class FrameCalculadora extends JFrame{
	public FrameCalculadora(){
		
		
		//posicion y tama�o
		//ventanaPrincipal.setBounds(x, y, width, height);
		setBounds(500, 200, 500, 500);
		
		//cerrar ventana
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		//visibilidad
		setVisible(true);

		//icono
		Toolkit sistema = Toolkit.getDefaultToolkit();
        Image icono = sistema.getImage("calculadora.png");
        setIconImage(icono);
        
        PanelCalculadora panel = new PanelCalculadora();
        add(panel);
	}
}