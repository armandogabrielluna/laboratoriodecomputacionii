package TrabajoPracticoLaCalculadora;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class PanelCalculadora extends JPanel{
	
	private JButton pantalla;
	private JPanel botoncitos;
	private boolean comienzo;
	private double resultado=0;
	private String ultimaOperacion;
	
	public PanelCalculadora() {
		comienzo = true;
		//creamos la "pantalla" de la calculadora mediante un boton desactivado
		setLayout(new BorderLayout());
		pantalla = new JButton("0");
		pantalla.setEnabled(false);
		add(pantalla, BorderLayout.NORTH);
		
		//los botones de la calculadora
		botoncitos = new JPanel();
		//definimos una grilla de 4x4
		botoncitos.setLayout(new GridLayout(4,4));
		//definimos un contenedor para nuestro panel
		add(botoncitos, BorderLayout.CENTER);
		
		//oyente para escuchar que boton es apretado y mostrarlo en la pantalla de la calculadora
		mostrarNumero oyenteNumero  = new mostrarNumero();
		operacion oyenteOperacion = new operacion();
		
		nuevoBoton ("7", oyenteNumero);
		nuevoBoton ("8", oyenteNumero);
		nuevoBoton ("9", oyenteNumero);
		nuevoBoton ("x", oyenteOperacion);
		
		nuevoBoton ("4", oyenteNumero);
		nuevoBoton ("5", oyenteNumero);
		nuevoBoton ("6", oyenteNumero);
		nuevoBoton ("-", oyenteOperacion);
		
		nuevoBoton ("1", oyenteNumero);
		nuevoBoton ("2", oyenteNumero);
		nuevoBoton ("3", oyenteNumero);
		nuevoBoton ("+", oyenteOperacion);
		
		nuevoBoton ("borrar", oyenteOperacion);
		nuevoBoton ("0", oyenteNumero);
		nuevoBoton ("=", oyenteOperacion);
		nuevoBoton ("/", oyenteOperacion);
		
		ultimaOperacion = "=";
	}
	
	private void nuevoBoton(String textoDelBoton, mostrarNumero oyente) {
		JButton boton = new JButton(textoDelBoton);
		boton.addActionListener(oyente);
		botoncitos.add(boton);
		
	}
	private void nuevoBoton(String textoDelBoton, operacion oyente) {
		JButton boton = new JButton(textoDelBoton);
		boton.addActionListener(oyente);
		botoncitos.add(boton);
		
	}
	
	private class mostrarNumero implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent accion) {
			//con el comando getActionCommand devolvemos el string que se usa como texto del boton
			String botonApretado = accion.getActionCommand();
			//logica para "suprimir" el 0 por defecto en la "pantalla" de la calculadora
			if(comienzo) {
				pantalla.setText(botonApretado);
				comienzo = false;
			}else {
				//eso lo mandamos al "boton" desactivado que funciona como pantalla 
				pantalla.setText(pantalla.getText()+botonApretado);
			}
		}
	}
	
	private class operacion implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent accion) {
			comienzo = true;
			String botonApretado = accion.getActionCommand();
			if(botonApretado.equals("borrar")) {
					pantalla.setText("0");
					resultado = 0;
			}else {
				calcular(Double.parseDouble(pantalla.getText()));
				ultimaOperacion = botonApretado;
			}
			
		}
	}

	private void calcular(double numero) {
		
		if(ultimaOperacion.equals("+")) {
			resultado = resultado + numero;
			pantalla.setText(Double.toString(resultado));
			
		}else if(ultimaOperacion.equals("-")) {
			resultado = resultado - numero;
			pantalla.setText(Double.toString(resultado));
			
		}else if(ultimaOperacion.equals("x")) {
			resultado = resultado * numero;
			pantalla.setText(Double.toString(resultado));
			
		}else if(ultimaOperacion.equals("/")) {
			if(numero == 0) {
				pantalla.setText("Math ERROR");
				resultado = 0;
			}else {
				resultado = resultado / numero;
				pantalla.setText(Double.toString(resultado));
			}
		}
		else if (ultimaOperacion.equals("=")) {
			resultado = numero;
		}
	}

	
}
